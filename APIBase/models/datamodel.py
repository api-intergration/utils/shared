from ..utils.extensions import db
from ..utils.utils_sqlalchemy import ResourceMixin


class device_model(db.Model,ResourceMixin):
	__tablename__ = 'devices'
	__table_args__ = {'extend_existing': True}
	id = db.Column(db.Integer, primary_key=True)
	manufacturer = db.Column(db.VARCHAR(100), unique=False)
	model_number = db.Column(db.VARCHAR(100), unique=False)
	SN_sys_id = db.Column(db.VARCHAR(100), unique=False)
	serial = db.Column(db.VARCHAR(100), unique=False)
	location = db.Column(db.VARCHAR(500), unique=False)
	name = db.Column(db.VARCHAR(100), unique=False)
	dnac_provisioned = db.Column(db.BOOLEAN, unique=False,default=False)
	admin_approved = db.Column(db.BOOLEAN, unique=False,default=False)

	@classmethod
	def find_by_id(cls, device):
		"""
		Find a Device by id. this would be used for find the device from the teams response

		:param id: ID Number
		:type id: int
		:return: device instance
		"""
		return device_model.query.filter(
			(device_model.id == device)).first()

	@classmethod
	def find_by_serial(cls, serial):
		"""
		Find a Device by serial. this would be used for find the device from the teams response

		:param id: ID Number
		:type id: int
		:return: device instance
		"""
		return device_model.query.filter(
			(device_model.serial == serial)).first()

	def serialize(self):
		return {
	        'id':self.id,
	        'manufacturer':self.manufacturer,
	        'model_number': self.model_number,
	        'SN_sys_id': self.SN_sys_id,
	        'serial' : self.serial,
	        'location': self.location,
	        'name': self.name,
	        'dnac_provisioned': self.dnac_provisioned,
	        'admin_approved': self.admin_approved

		}

