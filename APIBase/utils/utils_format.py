def to_markdown_table(pt):
	"""
	Print a pretty table as a markdown table

	:param py:obj:`prettytable.PrettyTable` pt: a pretty table object.  Any customization
	  beyond int and float style may have unexpected effects

	:rtype: str
	:returns: A string that adheres to git markdown table rules
	"""
	_junc = pt.junction_char
	if _junc != "|":
		pt.junction_char = "|"
	markdown = [row[1:-1] for row in pt.get_string().split("\n")[1:-1]]
	pt.junction_char = _junc
	return "\n".join(markdown)
