import os
def get_config():
    class Config:
        #SERVER_NAME=os.environ['FLASK_SERVER_NAME']
        OAUTH2_REFRESH_TOKEN_GENERATOR = True
        DEBUG = False
        FLASK_DEBUG = False
        # Flask-Restplus settings
        SWAGGER_UI_DOC_EXPANSION = 'list'
        RESTPLUS_VALIDATE = True
        RESTPLUS_MASK_SWAGGER = False
        ERROR_404_HELP = True

        """EXPLAIN_TEMPLATE_LOADING = True"""
        LOG_LEVEL = 'DEBUG'  # CRITICAL / ERROR / WARNING / INFO / DEBUG
        SECRET_KEY = 'CLUS-CISCO'
        WTF_CSRF_ENABLED = False
        # SQLAlchemy.
        """ username and pass in db_uri must match from .env file (POSTGRES_USER=) (POSTGRES_PASSWORD=) """
        if "LOCAL_SQL_CONNECTION" in os.environ:
             db_uri = os.environ['LOCAL_SQL_CONNECTION']
        else:
            db_uri = 'postgresql://db_user:clus123@postgres/apiserver'
        SQLALCHEMY_DATABASE_URI = db_uri
        SQLALCHEMY_TRACK_MODIFICATIONS = False
        TEAMS_BOT_TOKEN = os.environ['TEAMS_BOT_TOKEN']
        TEAMS_BOT_EMAIL = os.environ['TEAMS_BOT_EMAIL']
        TEAMS_BOT_ROOM_ID = os.environ['TEAMS_BOT_ROOM_ID']
        MESSAGE = False
        CARDS = False

    return Config
