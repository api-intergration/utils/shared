from objdict import ObjDict,JsonEncoder
import json
def convertDeviceList(data):
    response = []
    test1 = []
    item = ObjDict()
    item.Hostname = " "
    item.Ip_Address = " "
    item.Defice_Family = " "
    item.Model_Number = ""
    item.Serial_Number = ""
    item.Site = ""
    for device in data:
        for key in device:
            if key == "hostname":
                item.Hostname=device.hostname
            elif key == "managementIpAddress":
                item.Ip_Address = device.managementIpAddress
            elif key == "family":
                item.Defice_Family = device.family
            elif key == "platformId":
                item.Model_Number = device.platformId
            elif key == "serialNumber":
                item.Serial_Number=device.serialNumber
            elif key == 'location':
                item.Site = device.location
        response.append(json.dumps(item, cls = JsonEncoder))

        test1.append(item)
    return response





