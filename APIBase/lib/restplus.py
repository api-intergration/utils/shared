import logging
from flask import Blueprint
from flask_restplus import Api

log = logging.getLogger(__name__)
apiBlueprint = Blueprint('api', __name__, url_prefix='/api/v1')

api = Api(app=apiBlueprint,version='1.0',
            title='Network Device Management',
            description='Server to run network device imports into DNAC')

#@api.errorhandler
#def default_error_handler(self):
#    message = 'An unhandled exception occurred.'
#    log.exception(message)
#    config = get_config()
#    if not config.FLASK_DEBUG:
#        return {'message': message}, 500


#@api.errorhandler(NoResultFound)
#def database_not_found_error_handler(self,e):
#    log.warning(traceback.format_exc())
#    return {'message': 'A database result was required but none was found.'}, 404

