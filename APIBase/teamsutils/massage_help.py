#from utils_logging import apilogging
from webexteamssdk import webexteamssdkException
from ..utils.utils_teams import teams_api


def send(email):

	try:
		#apilogging.debug(
			#f"Token: {environ['WEBEX_TEAMS_ACCESS_TOKEN']}\n"
			#f"RoomID: {environ['TEAMS_BOT_ROOM_ID']}")

		msg =(
			"Gizmo bot is designed to alert you when new devices are received in Service Now\n"
			"After devices have been received the system will deploy the new device into DNAC\n"
			"Commands:\n"
			"/yes:<id> \t Approves Configuration of a new PnP device into DNA Center\n"
			"/device-health:<ipaddress> \t returns JSON formated device health\n"
			"/network-health \t Returns JSON formated network health\b"
			"/help \tThis message"
		)
		teams_api.messages.create(toPersonEmail=email,text=msg)
		return True
	except webexteamssdkException as e:
		#apilogging.debug(f"Message Failed for going to teams: {str(e)}")

		return False

